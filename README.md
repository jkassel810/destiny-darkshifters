## Overview

This is a website used to display player statistics and equipment information for the game Destiny (https://www.bungie.net/en/pub/AboutDestiny), which leverages the Bungie.net API. 

As part of my code I am also leveraging a destiny-client API wrapper created by Walter Carvalho.  You can find his Repo at: https://github.com/waltfy/destiny.


## Usage


The root page will bring direct you to a form in which you enter your Username.  This defaults for PSN users only for now but can easily be changed in the code.  This page is bare-bones right now but i will add much more functionality including integration with xbox live and psn login authentication.

/getMembership:username - is used to return the membership ID based on a username

/get_characters/:username - is used to return the character ID based on the membership ID which is based on the username

/get_characters/:username/:characterId - Since we now have the character ID, we can now pull character information including current equipment.

Since the result of hitting the bungie API returns equipment as a hash ID, we need to access an addtional API endpoint that Bungie provides.  This acts as a common database of inventory items including the icon, name, type, etc.  once i pull the hash ID of each inventory item that the character owns, i do a lookup against this API called InventoryItem in the code.  

## Motivation

One of the struggles in the game is being able to quickly and accuraly identify the proper armor and weopons to equip while playing the game depending on the scenario.  For example, you may want a specific armor's perks to help you beat others in a PvP cruicible match, and different armor/weopon perks for a PvE mission such as a Raid.  

The goal here touches on several aspects:

* Learn the coding language to build the RESTFUL interface using Node.js
* Understand how to call the Bungie API
* Display data returned from calling the API
* Analyze the data returned (i.e. - stats and perks of inventory)
* Build an interactive web site that allows a user to:
    - Retrieve data for their character 
    - Define custom equipment templates
    - Leverage auto-suggested/generated templates based on input parameters

## Installation

1. install node.js
    1. I did this by first installing NVM (yum install nvm)
    1. Run nvm instnode packages
1. install dependent packages
    1. npm install express
    1. npm install cookie-parser
    1. npm install morgan
    1. npm install formidable
    1. npm install sqlite3 //although not technically used anymore
1. run export BUNGIE_API_KEY='<yourapikey>'; node server.js production & 

(where the api key is the one you recieved from Bungie's Dev page)

## Contributors

* Jeff Kassel (https://bitbucket.org/jkassel810)
* Carlos Arias
