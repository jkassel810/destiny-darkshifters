var fs = require('fs');
var keyFile = '../api.key';

var apiKey = fs.readFileSync(keyFile).toString().split('\n');

//console.log(apiKey);
var destiny = require('destiny-client')(apiKey[0]);

//var destiny = require('destiny-client')(process.env.BUNGIE_API_KEY);

var express = require('express');
var app = express();
var cookieParser = require('cookie-parser')
var expressSession = require('express-session')
var favicon = require('serve-favicon');
var logger = require('morgan');
var formidable = require("formidable");

//DB info
//var file = "db/world_sql_content.content";
//var sqlite3 = require("sqlite3").verbose();
//var db = new sqlite3.Database(file);
//

app.use(favicon(__dirname + '/public/images/favicon.ico'));

app.use(logger('dev'));
app.use(express.static('public'));
app.use(cookieParser())
app.use(expressSession({
	secret: '1234567890QWERTY',
	resave: true,
	saveUninitialized: true
}))

app.use(express.static('public'));

app.get('/', function (req, res) {
   console.log(req.cookies);
   res.sendFile( __dirname + "/" + "form.html" );
})

app.get('/get_membership', function(req, res) {

var username = req.query.username;

res.redirect ('/get_membership/'+username);

})

var memberID = '';
var output = '';
var charIDs = [];
var myClass = '';

app.get('/get_membership/:username', function (req, hres) {
	hres.writeHead(200, {'content-type': 'text/html'});
	hres.write('<html><head><title>Darkshifters Destiny Gateway</title> <link rel="stylesheet" media="screen" href="/styles/styles.css" /></head><body>')
	destiny
          .Search({
            membershipType: 2,
            name: req.params.username
          })          
	  .then(function (res) {
            memberID = res[0]['membershipId']; //gets the membership ID
            hres.write('Membership ID: ' + memberID + '<br>');
	
	    output += '<a href="/get_characters/'+req.params.username+'">Get Character List</a>';
	    hres.end(output);
	  })
})


app.get('/get_characters/:username', function (req, hres) {
  
  var form = new formidable.IncomingForm();

  form.parse(req, function(err, fields, files) {
  
  hres.writeHead(200, {'content-type': 'text/html'});
  hres.write('<html><head><title>Darkshifters Destiny Gateway</title> <link rel="stylesheet" media="screen" href="/styles/styles.css" /></head><body>')
  hres.write('Here are your Characters based on your account for User <b> ' + req.params.username + '</b>: <br>');

	destiny
          .Search({
            membershipType: 2,
            name: req.params.username
          })
          .then(function (res) {
            memberID = res[0]['membershipId']; //gets the membership ID
            hres.write('Membership ID: ' + memberID + '<br>');
	
	
	destiny
	  .Account({
	    membershipType: 2,
       	    membershipId: memberID
    	  })
    	  .then(function (res) {
		console.log('memberID: ' +memberID);
	    //console.log(res['characters'][0]['characterBase']['peerView']['equipment']); 
	    	var util = require('util');
		//console.log(util.inspect(res, {shwoHidden: true, depth: null })); 
	    
	    output = '';
	    output += '<table><thead><tr><th>Character</th><th>Class</th><th>Light</th></tr></thead><tbody>';
	    res['characters'].forEach(function(myChar, i) {  //loops through each character section
		//console.log('chars: ' + myChar['characterBase']['characterId']);
	    charIDs.push(myChar['characterBase']['characterId']);  //identifies the characterID for that character
		output += '<tr>';
		//output += '<td>' + i + '</td>';
		//console.log(myChar['characterBase']);
		output += '<td><img src="http://bungie.net/'+res['characters'][i]['emblemPath']+'" height="42" width="42"></td>';
		switch (myChar['characterBase']['classType']){
			case 0:
				myClass = 'Titan';
				break;
			case 1:
				myClass = 'Hunter';
				break;
			case 2:
				myClass = 'Warlock';
				break;
			case 3: 
				myClass = 'Unknown';
				break;
			}
		output += '<td><a href="/get_characters/'+req.params.username+'/'+myChar['characterBase']['characterId']+'">' + myClass + '</a></td>';
		output += '<td>' + myChar['characterBase']['powerLevel'] + '</td></tr>';
	
                        });
		output += '</tbody></table></body></html>';
                hres.write(output, function() {
                        hres.end();	
		});

	  });
  });
  });
})


app.get('/get_characters/:username/:charID', function (req, hres) {
var eoutput = '';
var foutput = '';
var tierTypeName = '';
var itemTypeName = '';
var itemName = '';
var itemIcon = '';
var charRes = {};

hres.writeHead(200, {'content-type': 'text/html'});
hres.write('<html><head><title>Darkshifters Destiny Gateway</title> <link rel="stylesheet" media="screen" href="/styles/styles.css" /></head><body>')

	  destiny
          .Search({
            membershipType: 2,
            name: req.params.username
          })
          .then(function (res) {
            memberID = res[0]['membershipId']; //gets the membership ID
            //hres.write('Membership ID: ' + memberID + '<br>');
	    output += 'Membership ID: ' + memberID + '<br>';
	 	console.log('memberID: ' + memberID); 
	    destiny
	    .Character({
		membershipType: 2,
		membershipId: memberID,
		characterId: req.params.charID
	    })
	    .then(function (resChar) { 
		console.log ('charID: ' + req.params.charID);
		output = '';
		//console.log(resChar);
		output += '<table><thead><tr><th>Item</th><th>Value</th></tr></thead><tbody>';
		charRes = Object.create(resChar);
		//console.log ('res: ' + res);	
		//console.log (charRes);
		var dateLastPlayed= resChar['characterBase']['dateLastPlayed'];
		//console.log('dateLastPlayed: ' +res['characterBase']['dateLastPlayed']);
		var minutesPlayedThisSession = resChar['characterBase']['minutesPlayedThisSession'];
		var minutesPlayedTotal = resChar['characterBase']['minutesPlayedTotal'];
		output += '<tr><td>Date Last Played:</td><td>'+dateLastPlayed+'</td></tr>';
		output += '<tr><td>Minutes Played This Session:</td><td>'+minutesPlayedThisSession+'</td></tr>';
		output += '<tr><td>Minutes Played Total:</td><td>'+minutesPlayedTotal+'</td></tr>';
		output += '</tbody></table>';
		
		 hres.write(output, function() {
	           //hres.end();
        	 });
	    
	    //console.log(res['characterBase']['peerView']['equipment'][0]['itemHash']);
	    eoutput = '';
	    eoutput = '<table><thead><tr><th>Equip Icon</th><th>Equip Name</th><th>Equip Type</th><th>Equip Tier</th></tr></thead><tbody>';
	    hres.write(eoutput);
	    console.log(charRes);
	    for(var itemHash in charRes['characterBase']['peerView']['equipment']){
		//console.log(itemHash+": "+res['characterBase']['peerView']['equipment'][itemHash]['itemHash']);
		//console.log('itemhash: ' + itemHash);
		destiny
		.InventoryItem({
		  itemId: charRes['characterBase']['peerView']['equipment'][itemHash]['itemHash']
		})
		.then(function (resItem) {
		  tierTypeName = resItem['inventoryItem']['tierTypeName'];
		  itemTypeName = resItem['inventoryItem']['itemTypeName'];	
		  itemName = resItem['inventoryItem']['itemName'];
		  itemIcon = '<img src="http://bungie.net' + resItem['inventoryItem']['icon']+'"height="42" width="42">';
		  eoutput = '<tr><td>'+itemIcon+'</td><td>'+itemName+'</td><td>'+itemTypeName+'</td><td>'+tierTypeName+'</td></tr>';
		
		 hres.write(eoutput, function() {
             	  //hres.end();
            	 });	
	    });		
	    }
	    });
	    
	    eoutput = '</tbody></table>'				
	    hres.write(eoutput);
	    foutput += '</body></html>';
            /*hres.write(foutput, function() {
              hres.end();
	    });*/
	
	});
})
var environment = process.argv[2].toLowerCase();
//var serverPort = '';

envPorts = [ {'env':'jeffdev','port':'3001'},{'env':'carlosdev','port':'3002'},{'env':'integration','port':'3003'},{'env':'production','port':'3000'}];

for(var item in envPorts) {
	if (envPorts[item].env  == environment){
		serverPort = envPorts[item].port;
		break;
	}
	else if(item == envPorts.length){
		console.log('Please specify an appropriate environment and rerun the script...Exiting');
        	process.exit(1);
		
	}
}

var server = app.listen(serverPort, function () {
    var host = server.address().address
    var port = server.address().port

    console.log("Darkshifters Destiny app ["+environment+"] listening at http://%s:%s", host, port)
   })
